#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num, i = 1;
    printf(" Enter any number: ");
    scanf("%d", &num);
    printf(" Multiplication table of %d:  \n",num);

    while (i <= 12)
    {
        printf("%d * %d = %d\n",num,i,num*i);
        i++;
    }
    return 0;
}
